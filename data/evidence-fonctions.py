## globales ##
nombres = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
consomnesMaj= ['B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Z']
consomnesMin = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']
voyellesMaj = ['A', 'E', 'I', 'O', 'U', 'Y']
voyellesMin = ['a', 'e', 'i', 'o', 'u', 'y']
## end ##

## sansFonction ##
for val in nombres:
    print("La valeur est :", val)
    print("Son double vaut :", val * 2)
    print("Son quadruple vaut :", val * 4)
    print("Son décuple vaut :", val * 10)

for val in consomnesMaj:
    print("La valeur est :", val)
    print("Son double vaut :", val * 2)
    print("Son quadruple vaut :", val * 4)
    print("Son décuple vaut :", val * 10)

for val in consomnesMin:
    print("La valeur est :", val)
    print("Son double vaut :", val * 2)
    print("Son quadruple vaut :", val * 4)
    print("Son décuple vaut :", val * 10)

for val in voyellesMaj:
    print("La valeur est :", val)
    print("Son double vaut :", val * 2)
    print("Son quadruple vaut :", val * 4)
    print("Son décuple vaut :", val * 10)

for val in voyellesMin:
    print("La valeur est :", val)
    print("Son double vaut :", val * 2)
    print("Son quadruple vaut :", val * 4)
    print("Son décuple vaut :", val * 10)
## end ##

## avecFonction ##
def affichage(valeur_a_afficher):
    print("La valeur est :", valeur_a_afficher)
    print("Son double vaut :", valeur_a_afficher * 2)
    print("Son quadruple vaut :", valeur_a_afficher * 4)
    print("Son décuple vaut :", valeur_a_afficher * 10)
        
for val in nombres:
    affichage(val)
    
for val in consomnesMaj:
    affichage(val)

for val in consomnesMin:
    affichage(val)

for val in voyellesMaj:
    affichage(val)

for val in voyellesMin:
    affichage(val)
## end ##
