# Portée locale
nom = 'Gandhi'
def demonstration_locale(nom):
    """Affiche le contenu du dictionnaire locale
    à la fonction courante et souhaite la bienvenue
    au 'nom' passé en argument.

    Condition initiale : aucune
    Argument : la chaîne de caractère correspondant au nom
    Sortie/Retour : aucun
    """
    print('Dictionnaire local :', locals())
    print('Bienvenue', nom)

print('Dictionnaire global :', globals())
demonstration_locale('Mahatma')
