## nomenclature ##
def nom_de_la_fonction(argument1, argument2, argument3=valeur_par_defaut):
    """Description de la fonction :
          - son utilité,
          - la façon dont on l'emploie.

    Donner les conditions initiales pour qu'elle puisse
    être utilisée.
    Décrire les types et les arguments attendus.
    Donner la(les) valeur(s) et les conditions de sortie
    de la fonction.
    """
    ...
    instructions
    ...
    return valeur_de_retour  # Uniquement si la fonction renvoie une valeur
## end ##

## docstrings ##
>>> help(nom_de_la_fonction)
Help on function nom_de_la_fonction in module __main__:

nom_de_la_fonction(argument1, argument2, argument3=3)
    Description de la fonction :
          - son utilité,
          - la façon dont on l'emploie.
    
    Donner les conditions initiales pour qu'elle puisse
    être utilisée.
    Décrire les types et les arguments attendus.
    Donner la(les) valeur(s) et les conditions de sortie
    de la fonction.
## end ##

## retourSimple ##
def fonction_avec_retour():
    """Fonction permettant d'obtenir l'heure actuelle.

    Usage :
    >>> fonction_avec_retour()
    '17:11:49'
    >>> heure = fonction_avec_retour()
    >>> print(heure)
    '17:11:49'

    Conditions initiales : aucune
    Arguments : aucun
    Sortie/Retour : renvoie un str représentant l'heure sous la
    forme 'HH:MM:SS'
    actuelle du système.
    """
    import time
    heure = str()
    heure = time.strftime("%X")
    return heure
## end ##

## retourSimpleConditionnel ##
def fonction_avec_retour_conditionnel():
    """Fonction permettant de connaître la parité
    de l'heure actuelle.

    Usage :
    >>> fonction_avec_retour_conditionnel()
    'paire'
    >>> fonction_avec_retour_conditionnel()
    'impaire'

    Conditions initiales : aucune
    Arguments : aucun
    Sortie/Retour : renvoie un str représentant la parité de l'heure
    actuelle du système.
    """
    import time
    if int(time.time()) % 2 == 0:
        return "paire"
    else:
        return "impaire"
## end ##

## retourMultiple ##
def fonction_avec_retour_multiple():
    """Fonction permettant d'obtenir l'heure actuelle sous
    trois formes.

    Usage :
    >>> fonction_avec_retour_multiple()
    ('17:11:59', 1447603919.604904, 1447603919)
    >>> a, b, c = fonction_avec_retour_multiple()
    >>> a
    '17:11:59'
    >>> b
    1447603919.604904
    >>> c
    1447603919

    Conditions initiales : aucune
    Arguments : aucun
    Sortie/Retour : renvoie un tuple de valeurs (str,float,int)
    représentant respectivement l'heure actuelle du système
    """
    import time
    heureStr = time.strftime("%X")
    heureFloat = time.time()
    heureInt = int(heureFloat)
    return heureStr, heureFloat, heureInt
## end ##

## argumentRetourSimple ##
def fonction_avec_argument_et_retour(argument):
    """Fonction permettant de calculer une hausse de 50%
    sur la valeur passée en argument.

    Usage :
    >>> print(fonction_avec_argument_et_retour(15))
    22.5

    Conditions initiales : l'argument doit être de type int ou float
    Arguments : 1/ la valeur numérique à réhausser de 50%
    Sortie/Retour : renvoie un float résultat du calcul, None si les
    conditions initiales ne sont pas respectées
    """
    if isinstance(argument, int) or isinstance(argument, float):
        return argument + argument / 2
    else:
        return None
## end ##

## sansRetour ##
def fonction_sans_retour():
    """Fonction permettant d'afficher une valeur fixée, son double, son
    quadruple et son décuple.

    Usage :
    >>> fonction_sans_retour()
    La valeur est : 132
    Son double vaut : 264
    Son quadruple vaut : 528
    Son décuple vaut : 1320

    Conditions initiales : aucune
    Arguments : aucun
    Sortie : aucune
    """
    valeur_a_afficher = 132
    print("La valeur est :", valeur_a_afficher)
    print("Son double vaut :", valeur_a_afficher * 2)
    print("Son quadruple vaut :", valeur_a_afficher * 4)
    print("Son décuple vaut :", valeur_a_afficher * 10)
## end ##
