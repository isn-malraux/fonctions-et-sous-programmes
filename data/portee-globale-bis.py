# Autre exemple de portée globale
nom = 'DAVID'
prenom = 'Grégory'
def bonjour():
    global nom
    global prenom
    nom = 'LE TEXIER'
    prenom = 'Vincent'
    print('Bonjour', prenom, nom)

bonjour()
