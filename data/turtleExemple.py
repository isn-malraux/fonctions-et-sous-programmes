import turtle
import math

def maison():
    l = 100
    turtle.speed(1.3)
    turtle.showturtle()
    turtle.right(90)
    turtle.forward(l)
    turtle.left(90)
    turtle.forward(l)
    turtle.left(90)
    turtle.forward(l)
    turtle.left(45)
    turtle.fillcolor("red")
    turtle.begin_fill()
    turtle.forward(l * math.sqrt(2)/2)
    turtle.left(90)
    turtle.forward(l * math.sqrt(2)/2)
    turtle.left(135)
    turtle.forward(l)
    turtle.end_fill()
    turtle.fillcolor("black")
    turtle.hideturtle()

maison()
turtle.exitonclick()
