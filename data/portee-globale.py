# Portée globale
nom = 'Gandhi'
def demonstration_globale(nom):
    """Affiche le contenu du dictionnaire locale
    à la fonction courante et souhaite la bienvenue
    au 'nom' de la variable globale.

    Condition initiale : aucune
    Argument : la chaîne de caractère correspondant au nom
    Sortie/Retour : aucun
    """
    print('Dictionnaire local :', locals())
    print('Bienvenue', globals()['nom'])

print('Dictionnaire global :', globals())
demonstration_globale('Mahatma')
