## puissance ##
def puissance(x, y):
    """Élévation de l'argument 'x' à la puissance de
    l'argument 'y'.

    Usage :
    >>> print(puissance(2, 3))
    8
    >>> print(puissance(1234, 0))
    1

    Conditions initiales : 'x' et 'y' sont des entiers relatifs
    Arguments : x = la valeur à élever, y = la puissance d'élévation
    Sortie : un entier relatif ou None si conditions non respectées
    """
    if isinstance(x, int) and isinstance(y, int):
        return x**y
    else:
        return None
## end ##

## normalisation ##
def normalisation(Valeur, Minimum, Maximum):
    """Normalisation sur [0..1] de 'Valeur' depuis l'échelle
    [Minimum..Maximum].

    Usage :
    >>> print(normalisation(0, -4, 45))
    0.08163265306122448
    >>> print(normalisation(234, 0, 255))
    0.9176470588235294

    Conditions initiales : 'Valeur' est comprise en 'Minimum' et Maximum'
    et sont des nombres
    Arguments :
       Valeur : la valeur à normaliser,
       Minimum : la borne inférieure de l'échelle de base,
       Maximum : la borne supérieure de l'échelle de base.
    Sortie : un nombre flottant compris entre 0 et 1 ou None si
    les conditions ne sont pas respectées
    """
    if (isinstance(Valeur, int) or isinstance(Valeur, float)) and (isinstance(Minimum, int) or isinstance(Minimum, float)) and (isinstance(Maximum, int) or isinstance(Maximum, float)):
        if Minimum < Valeur < Maximum:
            return (Valeur - Minimum) / (Maximum - Minimum)
        else:
            return None
    else:
        return None
## end ##

## somme ##
def somme(debut, fin):
    """Somme des entiers compris entre les deux valeurs
    fournies en argument

    Usage :
    >>> print(somme(45, 55))
    550
    >>> print(somme(-3, 3))
    0

    Conditions initiales : 'debut' est strictement
    inférieure à 'fin', et elles sont des entiers relatifs
    Arguments :
       debut : l'entier de départ pour la suite
       fin : l'entier d'arrêt de la suite
    Sortie : un nombre entier ou None si les conditions
    ne sont pas respectées
    """
    if isinstance(debut, int) and isinstance(fin, int):
        if debut < fin:
            somme = 0
            for valeur in range (debut, fin + 1):
                somme += valeur
            return somme
        else:
            return None
    else:
        return None
## end ##

## biscornue ##
def biscornue(v):
    """Affiche le produit de la valeur entière 'v' et de 123,
    si et seulement si le résultat est pair, sinon afficher
    l'heure actuelle du système sous la forme HH:MM:SS

    Usage :
    >>> biscornue("n'importe quoi")
    >>> biscornue(654321)
    '17:11:49'
    >>> biscornue(123456)
    15185088

    Conditions initiales : 'v' est un entier relatif
    Arguments : v la valeur d'entrée
    Sortie : un nombre entier ou un str représentant l'heure
    ou None si les conditions ne sont pas respectées
    """
    if isinstance(v, int):
        resultat = v * 123
        if resultat % 2 == 0:
            return resultat
        else:
            import time
            return time.strftime('%X')
    else:
        return None
## end ##

"""
## biscornueUsage ##
>>> biscornue("n'importe quoi")
>>> biscornue(654321)
'17:11:49'
>>> biscornue(123456)
15185088
## end ##
"""

## double_et_decuple ##
def double_et_decuple(A, B):
    """Calcule le double de l'entier 'A' et le décuple du
    flottant 'B' et les retourne, ou None sinon

    Usage :
    >>> double_et_decuple(123, 12.34)
    (246, 123.4)
    >>> double_et_decuple(123, 12)
    (246, 120.0)
    >>> double_et_decuple('a', 12.34)
    (None, 123.4)
    >>> double_et_decuple(123, 'z')
    (246, None)
    >>> double_et_decuple('a', 'z')
    (None, None)

    Conditions initiales : 'A' et 'B' sont des nombres
    Arguments :
        A : un entier qui sera doublé
        B : un flottant qui sera décuplé
    Sortie : un tuple constitué du double de 'A' ou None et du
    décuple de 'B'ou none, ou le tuple (None,None) si les
    conditions ne sont pas respectées
    """
    if isinstance(A, int) or isinstance(A, float):
        A = int(A * 2)
    else:
        A = None
    if isinstance(B, int) or isinstance(B, float):
        B = float(B * 10)
    else:
        B = None
    return A, B
## end ##

"""
## double_et_decupleUsage ##
>>> double_et_decuple(123, 12.34)
(246, 123.4)
>>> double_et_decuple(123, 12)
(246, 120.0)
>>> double_et_decuple('a', 12.34)
(None, 123.4)
>>> double_et_decuple(123, 'z')
(246, None)
>>> double_et_decuple('a', 'z')
(None, None)
## end ##
"""

## losangeFixe ##
def losange():
    """Trace un losange centré en (0.0, 0.0) de
    coté 100 pixels

    Usage :
    >>> losange()

    Conditions initiales : aucune
    Arguments : aucun
    Sortie : aucune
    """
    import turtle
    import math
    cote = 100
    turtle.penup()
    turtle.setposition(0, -(cote * math.sqrt(2) / 2))
    turtle.left(45)
    turtle.pendown()
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.penup()
    turtle.setposition(0,0)
    turtle.setheading(0)
    turtle.pendown()
## end ##

## losangeVariable ##
def losange(largeur=100):
    """Trace un losange centré en (0.0, 0.0) de coté 'largeur'

    Usage :
    >>> losange()
    >>> losange(333)
    >>> losange(l=666)

    Conditions initiales : aucune
    Arguments : aucun
    Sortie : aucune
    """
    import turtle
    import math
    cote = largeur
    turtle.penup()
    turtle.setposition(0, -(cote * math.sqrt(2) / 2))
    turtle.left(45)
    turtle.pendown()
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.left(90)
    turtle.forward(cote)
    turtle.penup()
    turtle.setposition(0,0)
    turtle.setheading(0)
    turtle.pendown()
## end ##

## calcul ##
def calcul(operandeA, operandeB, operateur):
    """Effectue le calcul demandé via les arguments

    Usage :
    >>> print(calcul(23, 32, '+'))
    55
    >>> print(calcul(4, 66, 'choucroute'))
    70
    >>> print(calcul('a', 66, '-'))
    None

    Conditions initiales : les deux premiers arguments
    doivent être des nombres, le dernier une chaîne
    d'un caractère
    Arguments :
        operandeA : la première opérande de l'opération 'operateur'
        operandeB : la seconde opérande de l'opération 'operateur'
        operateur : l'opérateur déterminant l'opération à réaliser
    Sortie : un entier ou un flottant résultat de l'opération ou None
    si les conditions initiales ne sont pas respectées
    """
    if (isinstance(operandeA, int) or isinstance(operandeA, float)) and (isinstance(operandeB, int) or isinstance(operandeB, float)) and isinstance(operateur, str):
        if operateur == '/':
            return operandeA / operandeB
        elif operateur == 'x':
            return operandeA * operandeB
        elif operateur == '-':
            return operandeA - operandeB
        else:
            return operandeA + operandeB
    else:
        return None

opA = input("Première opérande : ")
opB = input("Seconde opérande : ")
print(calcul(opA, opB, '+'))
print(calcul(opA, opB, '-'))
print(calcul(opA, opB, 'x'))
print(calcul(opA, opB, '/'))
## end ##

## calcul2 ##
def calcul(operandeA, operandeB):
    """Effectue le calcul demandé sur les arguments via
    la variable globale 'operateur'

    Usage :
    >>> operateur = '+'
    >>> print(calcul(23, 32))
    55
    >>> operateur = 'choucroute'
    >>> print(calcul(4, 66))
    70
    >>> operateur = '-'
    >>> print(calcul('a', 66))
    None

    Conditions initiales : les deux premiers arguments
    doivent être des nombres
    Arguments :
        operandeA : la première opérande de l'opération
        operandeB : la seconde opérande de l'opération
    Sortie : un entier ou un flottant résultat de l'opération ou None
    si les conditions initiales ne sont pas respectées
    """
    global operateur
    if (isinstance(operandeA, int) or isinstance(operandeA, float)) and (isinstance(operandeB, int) or isinstance(operandeB, float)) and isinstance(operateur, str):
        if operateur == '/':
            return operandeA / operandeB
        elif operateur == 'x':
            return operandeA * operandeB
        elif operateur == '-':
            return operandeA - operandeB
        else:
            return operandeA + operandeB
    else:
        return None

opA = float(input("Première opérande : "))
opB = float(input("Seconde opérande : "))
operateur = '+'
print(calcul(opA, opB))
operateur = '-'
print(calcul(opA, opB))
operateur = 'x'
print(calcul(opA, opB))
operateur = '/'
print(calcul(opA, opB))
## end ##

## multiples ##
def mul2(nombre):
    """Vérifie si le 'nombre' passé en argument
    est multiple de 2 ou non

    Usage :
    >>> print(mul2(12))
    True
    >>> print(mul2(11))
    False
    >>> print(mul2(12.4))
    False
    >>> print(mul2('azerty'))
    False

    Conditions initiales : la variable 'nombre' doit en être un
    Arguments :
        nombre : nombre entier relatif
    Sortie : un booléen, True si le 'nombre' est multiple de 2,
    sinon False
    """
    if isinstance(nombre, int):
        return (nombre % 2 == 0)
    else:
        return False

def mul3(nombre):
    """Vérifie si le 'nombre' passé en argument
    est multiple de 3 ou non

    Usage :
    >>> print(mul3(12))
    True
    >>> print(mul3(11))
    False
    >>> print(mul3(12.3))
    False
    >>> print(mul3('azerty'))
    False

    Conditions initiales : la variable 'nombre' doit en être un
    Arguments :
        nombre : nombre entier relatif
    Sortie : un booléen, True si le 'nombre' est multiple de 3,
    sinon False
    """
    if isinstance(nombre, int):
        return (nombre % 3 == 0)
    else:
        return False

valeur = int(input("Entrer un entier : "))
if mul2(valeur):
    print(valeur, "est multiple de 2")

if mul3(valeur):
    print(valeur, "est multiple de 3")

if mul2(valeur) and mul3(valeur):
    print(valeur, "est multiple de 6")
## end ##
